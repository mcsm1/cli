# MCSM Command Line Interface

This software is a component of the
[Minecraft Server Manager](https://gitlab.com/mcsm1) pack.
See that page for more details.

Its purpose is to run commands on the computer that the server is running on.
It will be used by the monitor and website, but may also be manually used.

It will start the monitor once the computer launches.
Frequently, the monitor will query the CLI to see if it can perform a shutdown.
See `isActive` below for more details.
If the monitor deems it has been long enough, it will use the CLI to shutdown
the computer.

# Usage
This command may be ran with one of the following arguments:

Note: some commands return strings, and some return json for web use.

- `help`: string - shows usage info
- `isActive`: string - prints true if the system is active (cannot be shutdown)
- `shutdown`: string - shuts down the system, regardless of isActive status
- `lock`: json - creates a development mode (monitor and website is paused)
- `unlock`: json - resumes normal activity
- `startServer`: json - starts the server
- `getFullState`: json - prints all of the server info needed to run the website
- `whitelist NAME`: json - adds NAME to the server whitelist

Calling `getFullState` also ensures the monitor is running.

# Customization
The program expects a mcsm-cli.json file next to the executable.
This file is used for customization and must be formatted as such:

```json
{
    "ShutdownCommand": "sudo /sbin/shutdown now",
    "StartServerCommand": "java -Xmx4G -jar server.jar",
    "RconPort": "25575",
    "RconPassword": "ABCDEFG",
    "PathToMonitor": "./mcsm-monitor"
}
```

`ShutdownCommand` - string - the command ran to shut down the computer

`StartServerCommand` - string - the command ran to start the server.
It will automatically be ran using `nohup`.

`RconPort` - string - the local port used for rcon

`RconPassword` - string - the password for rcon

`PathToMonitor` - string - the path to the monitor tool

`PathToServerLogs` - string - the path to the server logs folder


The program also expects a server.json file next to the executable.
This file will hold display info about the server for the website.
It must be formatted as such:

```json
{
    "Name": "The Epic Server",
    "Edition": "Java",
    "Version": "1.20.2",
    "Description": "A server for me and my friends!"
}
```

All of these are simply dislay strings and should be self-explanatory.
