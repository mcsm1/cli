package main

import (
	"os"

	"gitlab.com/davidjmacdonald1/mcsm/cli/cmd"
	"gitlab.com/davidjmacdonald1/mcsm/cli/files"
)

func main() {
	args := os.Args[2:]
	opts := files.MustReadOptions()
	cmds := cmd.Init(opts, args)

	if len(os.Args) < 2 {
		cmds.PrintUsage()
		os.Exit(1)
	}

	c, ok := cmds[os.Args[1]]
	if !ok {
		cmds.PrintUsage()
		os.Exit(2)
	}

	c.Exec()
}
