package util

import (
	"os"
	"os/exec"

	"gitlab.com/davidjmacdonald1/mcsm/cli/files"
)

const LockFilePath = "mcsm.lock"

func IsActive(opts *files.Options) bool {
	return IsServerOn(opts) || IsLocked()
}

func IsLocked() bool {
	_, err := os.Stat(LockFilePath)
	return err == nil
}

func VerifyMonitor(opts *files.Options) error {
	stdout, err := exec.Command("pgrep", "-f", opts.PathToMonitor).Output()
	_, isExitError := err.(*exec.ExitError)
	if !isExitError {
		return err
	}

	if string(stdout) == "" {
		args := []string{opts.PathToMonitor, ">/dev/null", "2>&1", "&"}
		return exec.Command("nohup", args...).Start()
	}

	return nil
}
