package util

import (
	"fmt"
	"io"
	"os"
	"os/exec"
	"path"
	"regexp"
	"strconv"
	"strings"

	"github.com/jltobler/go-rcon"
	"gitlab.com/davidjmacdonald1/mcsm/cli/files"
)

func IsServerOn(opts *files.Options) bool {
	stdout, err := exec.Command("pgrep", "-f", opts.StartServerCommand).Output()
	if err != nil || string(stdout) == "" {
		return false
	}

	latest, err := readLatestLogEnding(opts)
	if err != nil {
		return false
	}

	if strings.Contains(
		latest,
		"[Server thread/INFO]: ThreadedAnvilChunkStorage: All dimensions are saved",
	) {
		exec.Command("kill", "-9", strings.TrimSpace(string(stdout))).Run()
		return false
	}
	return true
}

func IsServerLoaded(opts *files.Options) bool {
	_, err := sendRcon(opts, "help")
	return err == nil
}

func StartServer(opts *files.Options) error {
	parts := strings.Split(opts.StartServerCommand, " ")
	parts = append(parts, ">/dev/null", "2>&1", "&")

	cmd := exec.Command("nohup", parts...)
	cmd.Dir = "server"

	return cmd.Start()
}

func Whitelist(opts *files.Options, player string) error {
	_, err := sendRcon(opts, "whitelist add "+player)
	return err
}

func GetServerPlayers(opts *files.Options) ([]string, error) {
	res, err := sendRcon(opts, "list")
	if err != nil {
		return nil, err
	}

	res = strings.Split(res, ":")[1]
	players := []string{}

	for _, str := range strings.Split(res, ",") {
		s := strings.TrimSpace(str)

		if s != "" {
			players = append(players, s)
		}
	}

	return players, nil
}

func GetServerClock(opts *files.Options) (float32, error) {
	maxMins, err := getScoreboard(opts, "#ItHandler it_max")
	if err != nil {
		return 0, err
	}

	halfMins, err := getScoreboard(opts, "#ItHandler it_clock")
	if err != nil {
		return 0, err
	}

	return float32(maxMins) - .5*float32(halfMins), nil
}

func readLatestLogEnding(opts *files.Options) (string, error) {
	path := path.Join(opts.PathToServerLogs, "latest.log")
	latest, err := os.Open(path)
	if err != nil {
		return "", err
	}
	defer latest.Close()

	latest.Seek(0, io.SeekEnd)
	size, _ := latest.Seek(0, io.SeekCurrent)

	buffer := make([]byte, 1024)
	latest.Seek(size-min(size, int64(len(buffer))), io.SeekStart)
	_, err = latest.Read(buffer)
	if err != nil {
		return "", err
	}

	return string(buffer), nil
}

func getScoreboard(opts *files.Options, command string) (int, error) {
	command = fmt.Sprintf("scoreboard players get %s", command)
	res, err := sendRcon(opts, command)
	if err != nil {
		return 0, err
	}

	return parseScoreboard(res)
}

func parseScoreboard(res string) (int, error) {
	re := regexp.MustCompile(`(\d+)`)
	match := re.FindString(res)
	if match == "" {
		return 0, nil
	}

	return strconv.Atoi(match)
}

func sendRcon(opts *files.Options, command string) (string, error) {
	url := fmt.Sprintf("rcon://localhost:%s", opts.RconPort)
	client := rcon.NewClient(url, opts.RconPassword)

	res, err := client.Send(command)
	if err != nil {
		return "", err
	}

	return res, nil
}
