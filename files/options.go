package files

import (
	"encoding/json"
	"log"
	"os"
)

type Options struct {
	ShutdownCommand    string
	StartServerCommand string
	RconPort           string
	RconPassword       string
	PathToMonitor      string
	PathToServerLogs   string
}

func MustReadOptions() *Options {
	file, err := os.ReadFile("mcsm-cli.json")
	if err != nil {
		log.Fatal(err.Error())
	}

	opts := new(Options)
	err = json.Unmarshal(file, opts)
	if err != nil {
		log.Fatal(err.Error())
	}

	return opts
}
