package files

import (
	"encoding/json"
	"os"
)

type Info struct {
	Name        string
	Edition     string
	Version     string
	Description string
}

func ReadInfo() (*Info, error) {
	file, err := os.ReadFile("server.json")
	if err != nil {
		return nil, err
	}

	info := new(Info)
	return info, json.Unmarshal(file, info)
}
