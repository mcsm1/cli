package out

import (
	"encoding/json"
	"fmt"
	"os"

	"gitlab.com/davidjmacdonald1/mcsm/cli/files"
)

type Output struct {
	IsOn     bool
	IsLocked bool
	State    *State
	Info     *files.Info
	Error    string
}

type State struct {
	IsOn     bool
	IsLoaded bool
	Players  []string
	MinsLeft float32
}

func PrintError(err error) {
	Print(Output{IsOn: true, Error: err.Error()})
}

func PrintGood() {
	Print(Output{IsOn: true})
}

func Print(a any) {
	data, err := json.MarshalIndent(a, "", "    ")
	if err == nil {
		fmt.Println(string(data))
	} else {
		fmt.Printf("{\n    \"Error\": \"%s\"\n}\n", err.Error())
	}

	os.Exit(0)
}
