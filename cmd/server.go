package cmd

import (
	"gitlab.com/davidjmacdonald1/mcsm/cli/files"
	"gitlab.com/davidjmacdonald1/mcsm/cli/out"
	"gitlab.com/davidjmacdonald1/mcsm/cli/util"
)

func startServer(opts *files.Options) command {
	return command{
		func() {
			if !util.IsServerOn(opts) {
				err := util.StartServer(opts)
				if err != nil {
					out.PrintError(err)
				}
			}

			out.PrintGood()
		},
		"json",
		"starts the minecraft server",
	}
}

func whitelist(opts *files.Options, args []string) command {
	return command{
		func() {
			err := util.Whitelist(opts, args[0])
			if err != nil {
				out.PrintError(err)
			}

			out.PrintGood()
		},
		"json",
		"adds a player to the whitelist",
	}
}

func getFullState(opts *files.Options) command {
	return command{
		func() {
			output := out.Output{
				IsOn:     true,
				IsLocked: util.IsLocked(),
				State: &out.State{
					IsOn:     util.IsServerOn(opts),
					IsLoaded: util.IsServerLoaded(opts),
				},
			}

			err := util.VerifyMonitor(opts)
			if err != nil {
				output.Error = err.Error()
				out.Print(output)
			}

			if output.State.IsLoaded {
				output.State.Players, err = util.GetServerPlayers(opts)
				if err != nil {
					output.Error = err.Error()
					out.Print(output)
				}

				output.State.MinsLeft, err = util.GetServerClock(opts)
				if err != nil {
					output.Error = err.Error()
				}
			}

			info, err := files.ReadInfo()
			if err != nil {
				output.Error = err.Error()
				out.Print(output)
			}

			output.Info = info
			out.Print(output)
		},
		"json",
		"prints the full server status",
	}
}
