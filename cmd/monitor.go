package cmd

import (
	"fmt"
	"log"
	"os"
	"os/exec"
	"strings"

	"gitlab.com/davidjmacdonald1/mcsm/cli/files"
	"gitlab.com/davidjmacdonald1/mcsm/cli/out"
	"gitlab.com/davidjmacdonald1/mcsm/cli/util"
)

func isActive(opts *files.Options) command {
	return command{
		func() {
			fmt.Printf("%t", util.IsActive(opts))
		},
		"string",
		"prints true if the computer is actively being used",
	}
}

func shutdown(opts *files.Options) command {
	return command{
		func() {
			cmd := strings.Split(opts.ShutdownCommand, " ")
			err := exec.Command(cmd[0], cmd[1:]...).Run()
			if err != nil {
				log.Fatal(err.Error())
			}
		},
		"string",
		"shuts down the computer",
	}
}

func lock() command {
	return command{
		func() {
			file, err := os.Create(util.LockFilePath)
			if err != nil {
				out.PrintError(err)
			}
			defer file.Close()

			out.PrintGood()
		},
		"json",
		"prevents automatic shutdowns for debugging",
	}
}

func unlock() command {
	return command{
		func() {
			err := os.Remove(util.LockFilePath)
			if err != nil {
				out.PrintError(err)
			}

			out.PrintGood()
		},
		"json",
		"returns to a normal state",
	}
}
