package cmd

import (
	"fmt"
	"slices"
	"strings"

	"gitlab.com/davidjmacdonald1/mcsm/cli/files"
)

type command struct {
	Exec    func()
	resType string
	usage   string
}

type commands map[string]command

func (c commands) PrintUsage() {
	names := make([]string, 0, len(c))

	for name := range c {
		names = append(names, name)
	}

	slices.Sort(names)
	lines := []string{"Usage mcsm-cli CMD", "", "CMD: one of"}

	for _, name := range names {
		cmd := c[name]
		line := fmt.Sprintf("    %s: %s - %s", name, cmd.resType, cmd.usage)
		lines = append(lines, line)
	}

	fmt.Println(strings.Join(lines, "\n"))
}

func Init(opts *files.Options, args []string) commands {
	cmds := make(commands)

	cmds["help"] = command{cmds.PrintUsage, "string", "shows this usage info"}
	cmds["isActive"] = isActive(opts)
	cmds["shutdown"] = shutdown(opts)
	cmds["lock"] = lock()
	cmds["unlock"] = unlock()
	cmds["startServer"] = startServer(opts)
	cmds["getFullState"] = getFullState(opts)
	cmds["whitelist"] = whitelist(opts, args)

	return cmds
}
